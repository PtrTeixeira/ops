Changelog
=========



0.1.0 (2017-05-11)
------------------

### Added ###
- Common role: Installs Ubuntu's info MOTD & updates everything
- Docker role: Installs Docker (on Debian)
- Java role: Installs OpenJDK8-jre, latest
- Elasticsearch role: Installs Elasticsearch & Kibana (on Debian), both latest 5.x
- Webtier role: Pulls `Traefik` docker image. 


