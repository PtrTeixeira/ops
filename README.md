Ops
====

A handful of helpful Ansible roles that I use to provision a local VM to test 
out. This explains its appearance as a whole bunch of stuff lumped together, 
rather than a perhaps perferrable role-per-repo layout. 

Given that, this is in no way something that should be depended on. I am going 
to change this at any time and as I see fit. 

But thanks for looking!

